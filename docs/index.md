![](images/title.jpg)

This is the website for the 2019 NSF Workshop on Deep Learning and Software Engineering (DLSE) co-located with the 34th IEEE/ACM International Conference on Automated Software Engineering ([ASE'19](https://2019.ase-conferences.org)). 

## Community Report

The results of the workshop disucsisons are now available in the form of a community report drafted by the workshop organizers and steering committee. **We encourage contributions from the community to this report!!**

Please visit the GitLab repo for the report and create a Merge Request to contribute today!

**[Click Here to View the Report](https://gitlab.com/dlse-workshop/dlse-workshop-community-report)**

## Workshop Purpose

The goal of this workshop is to bring together an international group of researchers and practitioners who work at the intersection of software engineering and deep learning for an intensive period of discussion. There are clear instances where SE researchers could benefit from collaboration with DL researchers and vice versa. Thus, the overarching theme of this discussion will be related to the synergies that exist between research on SE and DL, and the most promising avenues of future work that should be prioritized to advance both fields.
 
#### Workshop Goals 

 * To identify aspects of the engineering processes of DL-based software systems that need support from SE researchers.
 * To identify aspects of traditional SE processes that DL-researchers could help support through research on new algorithms or representations.
 * To outline an agenda for future work at the intersection of SE and DL.
 * To help bring about new interdisciplinary collaborations.

#### Topics for Discussion
 
* Methods for effectively representing software engineering artifacts in DL models (e.g., sequences, graphs, etc.).
* SE tasks that exhibit opportunity for improvement or automation using DL models (e.g. code completion, program repair, documentation, code synthesis, etc.).
* Best practices for applying DL to SE tasks.
* Methods for effective testing of DL models and software systems.
* Methods for improving the interpretability of DL models and software systems.
* Methods for creating, managing, and testing data related to DL models and software systems.
* Privacy, safety, security, and ethical concerns related to DL models and software systems.
* Novel abstraction and representation of DL model engineering artifacts/states that allow innovative solutions to DL engineering challenges.
* Engineering challenges of intelligent systems that require inter-play of DL models and traditional software components (e.g., autonomous vehicle engineering, software applications allowing audio input).

For more specific topics, please see the list of topics for [Breakout Sessions](program.md#intro-b).

[Click Here to Download Opening Slides](https://www.dropbox.com/s/reoxc8a87qcrlux/NSF_SE_Workshop.pdf?dl=0)



-----

## Organizing Committee

![](images/Denys.png)  
 
[Denys Poshyvanyk](http://www.cs.wm.edu/~denys/index.html), William & Mary 


![](images/Baishakhi.png) 

[Baishakhi Ray](http://rayb.info), Columbia University

-----

## Sponsor

![](images/Sol.png) 
 
[Sol Greenspan](https://www.nsf.gov/staff/staff_bio.jsp?lan=sgreensp&org=NSF&from_org=NSF), National Science Foundation

-----

## Steering Committee

![](images/Prem.png) 

[Prem Devanbu](https://web.cs.ucdavis.edu/~devanbu/), University of California at Davis

![](images/Matt.png) 

[Matthew Dwyer](https://matthewbdwyer.github.io), University of Virginia

![](images/Mike.png) 

[Michael Lowry](https://ti.arc.nasa.gov/profile/lowry/), NASA

![](images/Xiangyu.png) 

[Xiangyu Zhang](https://www.cs.purdue.edu/homes/xyzhang/), Purdue University

![](images/Rishabh.png) 

[Rishabh Singh](https://rishabhmit.bitbucket.io), Google

![](images/sebastian.png) 

[Sebastian Elbaum](http://www.cs.virginia.edu/~se4ja/), University of Virginia

![](images/Kevin.png) 

[Kevin Moran](https://www.kpmoran.com), William & Mary



This page provides an overview of the workshop schedule. To see more detail about each event, please click on the event title. 

### Assignment for Breakout Discussion Groups

The workshop will have two sessions ([Session 2](#s2)) & ([Session 3](#s3)) that each feature 3 parallel breakout discussion groups. Each workshop attendee has been assigned to attend one breakout group during each of these sessions. You can find your group assignments either by going to the [Participants Page](participants.md) or the [Breakout Sessions](program.md) page.

## Sunday November 10th

### Reception <small>(5:00pm - 6:00pm)</small>

Time | Room | Event
-----|------|------
5:00pm - 6:00pm | Kensington 2 | Light Dinner

### Session 0 <small>(6:00pm - 8:00pm)</small>

Time | Room | Event
-----|------|------
6:00pm - 6:30pm | Kensington 1 | Lightning Talk 1: [*Debugging Deep Learning Models Using Program Analysis Techniques*](lightning-talks.md#lightning-talk-1) - Xiangyu Zhang, Purdue University
6:30pm - 7:00pm | Kensington 1 | Lightning Talk 2: [*Neural Program Synthesis*](lightning-talks.md#lightning-talk-2) - Rishabh Singh, Google
7:00pm - 7:30pm | Kensington 1 | Lightning Talk 3: [*When Deep Learning Met Code Search*](lightning-talks.md#lightning-talk-3) - Satish Chandra, Facebook
7:30pm - 8:00pm | Kensington 1 | Lightning Talk 4: [*Connecting Natural Language and Code using Deep Learning*](lightning-talks.md#lightning-talk-1) - Raymond Mooney, University of Texas at Austin

-----

## Monday November 11th

### Session 1 <small>(8:30am - 10:30am)</small>

Time | Room | Event
-----|------|------
8:30am - 8:45am | Cortez 3 | [Workshop Introduction](session-1.md#workshop-introduction) - Sol, Baishakhi, and Denys
8:45am - 9:15am | Cortez 3 | [Overview of DL4SE & SE4DL](session-1.md#workshop-logistics) - Denys
9:15am - 10:00am | Cortez 3 | [Participant Introductions](session-1.md#workshop-participant-introductions) - All Participants
10:00am - 10:30am | Cortez 3 | [Introduction of Breakout Group Topics](session-1.md#introduction-of-breakout-groups) - Session Leads

### Coffee <small>(10:30am - 10:45 am)</small>


### Session 2 <small>(10:45am - 1:00pm)</small>
<a name="s2"></a>

Time | Room | Event
-----|------|------
10:45am - 11:45am | Normal Heights | Breakout Group 1: [Deep Learning for Software Engineering](program.md#breakout-group-1)
10:45am - 11:45am | University Heights | Breakout Group 2: [Verification & Validation of Deep Learning Systems](program.md#breakout-group-2)
10:45am - 11:45am | Cortez 3 | Breakout Group 3: [Development & Deployment Challenges for Deep Learning Systems](program.md#breakout-group-3)
11:45am - 1:00pm | Cortez 3 | [Plenary Discussion 1](plenary.md#plenary-session-1)

### Lunch <small>(1:00pm - 2:00pm)</small>

Time | Room | Event
-----|------|------
1:00pm - 2:00pm | Kensington Ballroom/Terrace | Lunch (Co-Located with ASE'19)

### Session 3 <small>(2:00pm - 3:45pm)</small>
<a name="s3"></a>

Time | Room | Event
-----|------|------
2:00pm - 3:00pm | Normal Heights | Breakout Group 4: [Maintenance of Deep Learning Systems](program.md#breakout-group-4)
2:00pm - 3:00pm | University Heights | Breakout Group 5: [Testing of Deep Learning Systems](program.md#breakout-group-5) 
2:00pm - 3:00pm | Cortez 3 | Breakout Group 6: [Deep Learning for Code Generation](program.md#breakout-group-6)
3:00pm - 3:45pm | Cortez 3 | [Plenary Discussion 2](plenary.md#plenary-session-2)

### Coffee <small>(3:45pm - 4:00pm)</small>

### Session 4 <small>(4:00pm - 6:00pm)</small>

Time | Room | Event
-----|------|------
4:00pm - 4:30pm | Cortez 3 | Continuation of [Plenary Discussion 2](plenary.md#plenary-session-2)
4:30pm - 6:00pm | Cortez 3 | Plenary Discussion 3: [Cross-Cutting Concerns for DL & SE](plenary.md#plenary-session-3)

### Dinner <small>(7:00pm - 9:00pm)</small>

Time | Room | Event
-----|------|------
7:00pm - 9:00pm | Kensington 2 | Workshop Dinner


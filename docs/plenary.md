## Plenary Session 1

### Details

* **Topic:** *Breakout Groups 1, 2, & 3*
* **Time:** 11:45am - 1:00pm
* **Room:** Cortez 3
* **Session Leads:** [Denys Poshyvanyk](http://www.cs.wm.edu/~denys/index.html), [Baishaki Ray](http://rayb.info/)
* **Session Scribe:** [Kevin Moran](https://www.kpmoran.com/)
* **All Participants** 

**Discussion Notes**

* [Link to Plenary Session 1 Google Doc](https://docs.google.com/document/d/1vIDdtL6VKtT3O7AaV3wJV4tx8IjYbYqsjhyDrRhCvUs/edit?usp=sharing)
* [Link to Plenary Session 1 ScratchPad Doc](https://docs.google.com/document/d/1a_EcpG494V14pNBnqeKhH-RxX0o7axSPZGbXXEjgsp0/edit?usp=sharing)
-----

## Plenary Session 2

### Details

* **Topic:** *Breakout Groups 4, 5, & 6*
* **Time:** 3:00pm - 3:45pm; 4:00-4:30pm
* **Room:** Cortez 3
* **Session Leads:** [Denys Poshyvanyk](http://www.cs.wm.edu/~denys/index.html), [Baishaki Ray](http://rayb.info/)
* **Session Scribes:** [Kevin Moran](https://www.kpmoran.com/), [Mike Lowry](https://ti.arc.nasa.gov/profile/lowry/), and [Matthew Dwyer](https://matthewbdwyer.github.io/)
* **All Participants** 

**Discussion Notes**

* [Link to Plenary Session 2 Google Doc](https://drive.google.com/open?id=1tp067fKEK6KDjU2hiI0s_1iM9OfTEAmZ4pOmzKxgAeo)
* [Link to Plenary Session 2 ScratchPad Doc](https://docs.google.com/document/d/12Rj90bRGc4DLCvF3wk0tVz2ICoFmPzr5AKCFCUMolxc/edit?usp=sharing)
-----

## Plenary Session 3

### Details

* **Topic:** *Cross Cutting Topics in Deep Learning and Software Engineering*
* **Time:** 4:30pm - 6:00pm
* **Room:** Cortez 3
* **Session Lead:** [Denys Poshyvanyk](http://www.cs.wm.edu/~denys/index.html)
* **Session Scribes:** [Xiangyu Zhang](https://www.cs.purdue.edu/homes/xyzhang/), [Kevin Moran](https://www.kpmoran.com/)
* **All Participants** 

### Discussion Points

In DL-based systems it is very difficult to control where and how
information is stored (knowledge is stored as weights of the network
in all the layers with some specialization in specific regions of NN).
Data safety regulations (e.g., European General Data Protection
Regulation) may require anonymized/aggregated statistics potentially
impacting the performance of the system, trouble-shooting and data
exploration.


1.  “Explainability of DL models”
    *  Deep learning models raise ethical questions when applied to specific
tasks, if we are unable to explain why a specific classification
occurred then we are powerless to control the moral and ethical
dilemmas that DL introduces.
    *  To progress the effectiveness of DL models we need to understand the
inter-workings of the DL components (neurons, backprop, gradient
descent) but more importantly, how they impact and influence one
another leading to an input resulting in an output.
    *  Reduction of complexity is an important concept in SE. Since we are
unable to determine exactly what and why certain features are valued
to create a relationship between the input and output, we are also
unable to reduce the complexity of the model while maintaining its
effectiveness.



2.  “Security implications”
    *  Deep learning poses a grave security threat using adversarial
examples, which are small perturbations to an input which cause a very
unexpected output.
    *  Security has two research directions: 1. the process of creating new,
adversarial data examples to confuse and break DL models and 2. the
process of defending against these adversarial examples using unique
training techniques.
    *  Security is also directly tied to the variety of data given for
training, therefore, unexpected or unique instances (even when not
intentionally generated) can be problematic. When datapoints fall out
of their expected distribution into outlier territory, there is no
guarantee that the model will react as we expect which can have
security risks for whatever the task is that is model dependent.



3.  “Testing for Bias in training data”
    *  DL models are data dependent, but humans don't always collect data
with equality and a removal of bias in mind, therefore, we can expect
DL models to react in similar ways when trained on this type of data
    *  Since we are unable to control the features the model extracts, or
understand why they extract them, models have free reign to consider
discriminatory variables in order to make classifications.
    *  Testing for bias data is complex because it is dependent on human
judgement. Ideally, we could train a DL model to tell us when data is
bias, but that would require having a dataset of what is considered
diverse and without bias. We desperately need an objective way to
determine when data is unbias and more importantly understand how bias
data may affect classification of DL models.

4. Reviewing expertise for papers that sit at the intersection of AI/DL & SE
    *  As AI/DL related papers in SE become a more popular topic, we need to address issues related to reviewing expertise.

**Discussion Notes**

* [Link to Plenary Session 3 Google Doc](https://docs.google.com/document/d/1q7jdQaloKRTvav76m0X274SCOiOQPhNOmRcEGM5Pjn0/edit?usp=sharing)
* [Link to Plenary Session 3 ScratchPad Doc](https://docs.google.com/document/d/1ZA2TOckTVyrPqRCix7JNkxLZ8OMu-kRK-6wO3-DVhMU/edit?usp=sharing)

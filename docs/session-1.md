### Workshop Introduction

* **Presenters:** Sol Greenspan, Baishakhi Ray, and Denys Poshyvanyk
* **Time:** 8:30am - 8:45am
* **Room:** Cortez 3
* **Description:** The organizing committee will open the workshop and introduce its purpose and themes.

-----

### Overview of DL4SE and SE4DL

* **Time:** 8:45am - 9:15am
* **Room:** Cortez 3
* **Presenters:** Baishakhi Ray, and Denys Poshyvanyk
* **Description:** Denys Poshyvanyk will give an overview of that state of research in DL4SE as well as challenges on the horizon for DL-driven software development and how SE4DL research could help to solve these challenges.
* [Click Here to Download Talk Slides](https://www.dropbox.com/s/reoxc8a87qcrlux/NSF_SE_Workshop.pdf?dl=0)


-----

### Workshop Participant Introductions

* **Time:** 9:15am - 10:00am
* **Room:** Cortez 3
* **Presenters:** All Participants
* **Description:** Each workshop participant will be given 2 minutes to introduce themselves to the other attendees. Please have a short 2 minute oral introduction prepared. Time Limits will be enforced.

-----

### Introduction of Breakout Groups

* **Time:** 10:00am - 10:30am
* **Room:** Cortez 3
* **Presenters:** All Participants
* **Description:** Each Breakout session leader will introduce thier own session, including its themes and goals.
    *  **Breakout Session 1**: [*Deep Learning for Software Engineering*](#b1)
        * Session Lead: [Prem Devanbu](https://web.cs.ucdavis.edu/~devanbu/)
    * **Breakout Session 2**: [*Verification & Validation of Deep Learning Systems*](#b2)
        * Session Lead: [Matt Dwyer](https://matthewbdwyer.github.io/)
    * **Breakout Session 3**: [*Development & Deployment Challenges for Deep Learning Systems*](#b3)
        * Session Lead: [Mike Lowry](https://ti.arc.nasa.gov/profile/lowry/)
    * **Breakout Session 4**: [*Maintenance of Deep Learning Systems*](#b4)
        * Session Lead: [Sebastian Elbaum](http://www.cs.virginia.edu/~se4ja/)
    * **Breakout Session 5**: [*Testing of Deep Learning Systems*](#b5)
        * Session Lead: [Xiangyu Zhang](https://www.cs.purdue.edu/homes/xyzhang/)
    * **Breakout Session 6**: [*Deep Learning for Code Generation*](#b6)
        * Session Lead: [Rishab Singh](https://rishabhmit.bitbucket.io/)
    * **Plenary Discussion**: [*Cross Cutting Concerns for Deep Learning & Software Engineering*](#p3)
        * Session Lead: [Denys Poshyvanyk](http://www.cs.wm.edu/~denys/index.html)
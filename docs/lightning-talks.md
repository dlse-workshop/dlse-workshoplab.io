## Lightning Talks 

### Lightning Talk 1

*  **Title:** *Debugging Deep Learning Models Using Program Analysis Techniques*
*  **Presenter:** [Xiangyu Zhang](https://www.cs.purdue.edu/homes/xyzhang/), Purdue University
*  **Time:** 6:00pm - 6:30pm
*  **Room:** Kensington 1

-----

### Lightning Talk 2

*  **Title:** *Neural Program Synthesis*
*  **Presenter:** [Rishabh Singh](https://rishabhmit.bitbucket.io/), Google
*  **Time:** 6:30pm - 7:00pm
*  **Room:** Kensington 1


-----

### Lightning Talk 3

*  **Title:** *When Deep Learning Met Code Search*
*  **Presenter:** [Satish Chandra](https://research.fb.com/people/chandra-satish/), Facebook
*  **Time:** 7:00pm - 7:30pm
*  **Room:** Kensington 1


-----

### Lightning Talk 4

*  **Title:** *Connecting Natural Language and Code using Deep Learning*
*  **Presenter:** [Raymond Mooney](https://www.cs.utexas.edu/~mooney/), University of Texas at Austin
*  **Time:** 7:30pm - 8:00pm
*  **Room:** Kensington 1


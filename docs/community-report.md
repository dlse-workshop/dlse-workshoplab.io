## Community Report

The results of the workshop disucsisons are now available in the form of a community report drafted by the workshop organizers and steering committee. **We encourage contributions from the community to this report!!**

Please visit the GitLab repo for the report and create a Merge Request to contribute today!

**[Click Here to View the Report](https://gitlab.com/dlse-workshop/dlse-workshop-community-report)**